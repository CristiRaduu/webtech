
const FIRST_NAME = "Cristian Marian";
const LAST_NAME = "Radu";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
 
    cache.getCache = function() {
            return this;
    }

    
    cache.pageAccessCounter = function(PageName='home') {
            if (Object.keys(cache).includes(PageName.toLowerCase()))
                cache[PageName.toLowerCase()]++;
            else
                cache[PageName.toLowerCase()] = 1;
    }
 
    return cache;
 }

 
module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

